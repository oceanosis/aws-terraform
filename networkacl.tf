# Network ACLs

resource "aws_network_acl" "dmz-public" {
  vpc_id = "${aws_vpc.main.id}"
  subnet_ids = ["${aws_subnet.main-public-1.id}", "${aws_subnet.main-public-2.id}" ]
  tags {
 	Name = "Public-ACL"
  }

}

resource "aws_network_acl" "dmz-private" {                                                       
  vpc_id = "${aws_vpc.main.id}"                                                                 
  subnet_ids = ["${aws_subnet.main-private-1.id}", "${aws_subnet.main-private-2.id}" ]            
  tags {                                                                                        
        Name = "Private-ACL"                                                                     
  }                                                                                             
                                                                                                
}                                                                                               
resource "aws_network_acl_rule" "dmz-public-1" {

  network_acl_id = "${aws_network_acl.dmz-public.id}"
    protocol = "tcp"
    egress  = true
    rule_number = 100
    rule_action = "allow"
    cidr_block =  "0.0.0.0/0"
    from_port = 22
    to_port = 22
}

resource "aws_network_acl_rule" "dmz-public-2" {
  network_acl_id = "${aws_network_acl.dmz-public.id}"
    protocol = "tcp"
    egress  = true
    rule_number = 101
    rule_action = "allow"
    cidr_block =  "0.0.0.0/0"
    from_port = 80
    to_port = 80
}
resource "aws_network_acl_rule" "dmz-public-3" {
  network_acl_id = "${aws_network_acl.dmz-public.id}"
    protocol = "tcp"
    egress  = true
    rule_number = 102
    rule_action = "allow"
    cidr_block =  "0.0.0.0/0"
    from_port = 443
    to_port = 443
}
resource "aws_network_acl_rule" "dmz-public-4" {
  network_acl_id = "${aws_network_acl.dmz-public.id}"
    protocol = "tcp"
    egress  = true
    rule_number = 103
    rule_action = "allow"
    cidr_block =  "0.0.0.0/0"
    from_port = 1024
    to_port = 65535
}

resource "aws_network_acl_rule" "dmz-private-1" {             
  network_acl_id = "${aws_network_acl.dmz-private.id}"        
    protocol = "-1"                                           
    egress  = true                                            
    rule_number = 100                                         
    rule_action = "allow"                                     
    cidr_block =  "10.0.0.0/16"                               
    from_port = 0                                             
    to_port = 0                                               
}                                                             
resource "aws_network_acl_rule" "dmz-private-2" {         
  network_acl_id = "${aws_network_acl.dmz-private.id}"    
    protocol = "-1"                                       
    egress  = false                                     
    rule_number = 101                                     
    rule_action = "allow"                                 
    cidr_block =  "10.0.0.0/16"                           
    from_port = 0                                         
    to_port = 0                                           
}                                                         
