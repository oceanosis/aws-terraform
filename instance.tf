resource "aws_instance" "web" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.public-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.mykeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.bootstrap-script.rendered}"
  tags {

	Name = "Web Server"
  }
}


resource "aws_instance" "test-web" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-private-2.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.private-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.mykeypair.key_name}"

  # user data
  # user_data = "${data.template_cloudinit_config.internal-script.rendered}"
  tags {

        Name = "Test Server"
  }
}
