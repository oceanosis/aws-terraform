# Public Security Group
resource "aws_security_group" "public-sg" {
  vpc_id = "${aws_vpc.main.id}"
  name = "public-sg"
  description = "security group that allows ssh, http, https as ingress traffic and all egress traffic"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  } 
    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
tags {
    Name = "public-sg"
  }
}

# Private Security Group
resource "aws_security_group" "private-sg" {
  vpc_id = "${aws_vpc.main.id}"
  name = "private-sg"
  description = "security group that allows only internal ingress traffic and all egress traffic"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["10.0.0.0/16"]
  }

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["10.0.0.0/16"]
  }
  tags {
    Name = "private-sg"
  }
}
