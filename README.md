# AWS-Terraform

- Creation of AWS Environment via Terraform 

* Download Terraform and put that under /usr/local/bin/ and check path accordingly.
* Clone terraform script 
* Create a user (terraform) in AWS IAM with admin access
* Download access and secret key.
* Create gitignore file and put aws key filename in that file.
* "terraform plan -out terraform.plan"
* "terraform apply terraform.plan"
* "terraform destroy"
